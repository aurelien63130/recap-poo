<?php
    class ArticleController{

        private $articleManager;

        public function __construct(){
            $this->articleManager = new ArticleManager();
        }

        public function listElement(){
            // Sélectionner nos objets dans la BDD
            $articles = $this->articleManager->getAll();

            require 'Vue/Article/list.php';
        }

        public function getOne($id){
            // Selectionner mon objet article
            $article = $this->articleManager->getOne($id);
            // Afficher une vue de détail
            require 'Vue/Article/detail.php';
        }

        public function add(){
            $errors = [];

            if($_SERVER["REQUEST_METHOD"] == 'POST'){
                $errors = $this->checkForm();

                if(count($errors) == 0){
                    $article = new Article(null, $_POST["titre"], $_POST["photo"], $_POST["description"]);
                    $this->articleManager->add($article);
                    header("Location: index.php?controller=article&action=list");
                }
                // Puis je redirige mon utilisateur

            }

            // Enregistre les données si le formulaire est valide
            // Afficher le formulaire
            require 'Vue/article/form.php';
        }

        public function remove($id){
            // Appeler mon manager pour supprimer l'entrée avec cet id
            $this->articleManager->remove($id);
            // rediriger mon utilisateur

            header("Location: index.php?controller=article&action=list");
        }

        public function edit($id){
            $errors = [];

            //Selectionne les données a afficher dans le formulaire
            $article = $this->articleManager->getOne($id);

            if($_SERVER["REQUEST_METHOD"] == "POST"){
                // Vérifie les champs utilisateurs
                $errors = $this->checkForm();

                if(count($errors) == 0){
                    $article->setDescription($_POST["description"]);
                    $article->setPhoto($_POST["photo"]);
                    $article->setTitre($_POST["titre"]);
                    $this->articleManager->update($article);

                    header("Location: index.php?controller=article&action=list");
                }

            }



            // Affiche le formulaire
            require 'Vue/Article/form.php';
        }

        private function checkForm(){
            $errors = [];

            if(empty($_POST["titre"])){
                $errors[] = 'Veuillez saisir un titre !';
            }

            if(empty($_POST["photo"])){
                $errors[] = 'Veuillez saisir une photo !';
            }

            if(empty($_POST["description"])){
                $errors[] = 'Veuillez saisir une description !';
            }

            return $errors;
        }
    }
?>