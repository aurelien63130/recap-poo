<?php
require 'Model/Article.php';
require 'Model/Manager/DbManager.php';
require 'Model/Manager/ArticleManager.php';
require 'Controller/ArticleController.php';

if(!isset($_GET["controller"]) && !isset($_GET["action"])){
    header("Location: index.php?controller=article&action=list");
}


if($_GET["controller"] == 'article'){
    $controller = new ArticleController();
}

if($_GET["action"] == 'list'){
    $controller->listElement();
}

if($_GET["action"] == 'detail' && isset($_GET["id"])){
    $controller->getOne($_GET["id"]);
}

if($_GET["action"] == "add"){
    $controller->add();
}

if($_GET["action"] == 'remove' && isset($_GET["id"])){
    $controller->remove($_GET["id"]);
}

if($_GET["action"] == "edit" && isset($_GET["id"])){
    $controller->edit($_GET["id"]);
}

?>