<html>
<head>
    <?php include 'Vue/Parts/global_stylesheets.php'; ?>
</head>
<body>



<div class="container">
    <?php include 'Vue/Parts/menu.php'; ?>

    <h1>Détail de l'article <?php echo($article->getTitre());?> !</h1>

    <a href="index.php?controller=article&action=list" class="btn btn-success">Retour au listing</a>
    <div class="row">
        <div class="col-md-5">
            <img class="img-thumbnail" src="<?php echo($article->getPhoto());?>" alt="image de l'article <?php echo($article->getTitre());?>">
        </div>
        <div class="col-md-7">
            <?php
                echo($article->getDescription());
            ?>
        </div>
    </div>



</div>

<?php include 'Vue/Parts/global_scripts.php'; ?>
</body>
</html>