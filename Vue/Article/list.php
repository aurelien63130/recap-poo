<html>
<head>
    <?php include 'Vue/Parts/global_stylesheets.php'; ?>
</head>
<body>



<div class="container">
    <?php include 'Vue/Parts/menu.php'; ?>

    <a class="btn btn-success" href="index.php?controller=article&action=add">Ajouter un article</a>
    <h1>Affichage de ma liste d'articles !</h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Titre</th>
            <th scope="col">Photo</th>
            <th scope="col">Description</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach ($articles as $article)
            {
        ?>
        <tr>
            <th scope="row"><?php echo($article->getId()); ?></th>
            <td><?php echo($article->getTitre()); ?></td>
            <td><img class="img-thumbnail" src="<?php echo($article->getPhoto());?>"/></td>
            <td><?php echo($article->getDescription()); ?></td>
            <td>
                <a href="index.php?controller=article&action=detail&id=<?php echo($article->getId());?>">Voir en détail !</a>
                <a href="index.php?controller=article&action=remove&id=<?php echo($article->getId());?>">Supprimer !</a>
                <a href="index.php?controller=article&action=edit&id=<?php echo($article->getId());?>">Editer !</a>
            </td>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>

</div>

<?php include 'Vue/Parts/global_scripts.php'; ?>
</body>
</html>