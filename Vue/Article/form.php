<html>
<head>
    <?php include 'Vue/Parts/global_stylesheets.php'; ?>
</head>
<body>


<div class="container">
    <?php include 'Vue/Parts/menu.php'; ?>

    <?php
        if(isset($article)){
            echo('<h1>Edition de l\'article '.$article->getTitre().' !</h1>');
        } else {
            echo('<h1>Ajouter un nouvel article !</h1>');
        }
    ?>


    <a href="index.php?controller=article&action=list" class="btn btn-success">Retour au listing</a>
    <div class="row">
        <form method="post">
            <div class="form-group">
                <label for="titre">Titre de l'article</label>
                <input name="titre"
                       <?php
                       if(isset($article)){
                           echo('value="'.$article->getTitre().'"');
                       }
                       ?>
                       type="text" class="form-control" id="titre" placeholder="Veuilles saisir le titre">
            </div>
            <div class="form-group">
                <label for="photo">Url de l'image</label>
                <input
                    <?php
                    if(isset($article)){
                        echo('value="'.$article->getPhoto().'"');
                    }
                    ?>
                    name="photo" type="text" class="form-control" id=photo" placeholder="Veuilles saisir l'utl de l'image">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" class="form-control" id="description" placeholder="Veuilles saisir la description">
                     <?php
                     if(isset($article)){
                         echo($article->getDescription());
                     }
                     ?>
                </textarea>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <?php include 'Vue/Parts/error-display.php'; ?>

    </div>


</div>

<?php include 'Vue/Parts/global_scripts.php'; ?>
</body>
</html>