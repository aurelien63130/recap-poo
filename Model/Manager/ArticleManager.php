<?php
class ArticleManager extends DbManager {
    public function getAll(){
        $arrayObjects = [];
        $req = $this->bdd->prepare("SELECT * FROM article");
        $req->execute();
        $res = $req->fetchAll();

        foreach ($res as $elem){
            $article = new Article($elem["id"], $elem["titre"],
                $elem["photo"], $elem["description"]);
            $arrayObjects[] = $article;
        }

        return $arrayObjects;
    }

    public function getOne($id){
        $req = $this->bdd->prepare("SELECT * FROM article WHERE id = :article");
        $req->execute(["article"=> $id]);

        $resultat = $req->fetch();

        $article = new Article($resultat["id"], $resultat["titre"],
            $resultat["photo"], $resultat["description"]);

        return $article;
    }

    public function add(Article $article)
    {
        $req = $this->bdd->prepare("INSERT INTO article (titre, photo, description)
        VALUES (:titre, :photo, :description)");

        $req->execute([
            "titre"=> $article->getTitre(),
            'photo'=> $article->getPhoto(),
            'description'=> $article->getDescription()
        ]);

    }

    public function remove($id)
    {
        $req = $this->bdd->prepare("DELETE FROM article WHERE id = :id");
        $req->execute(["id"=> $id]);
    }

    public function update(Article $article)
    {
        $req = $this->bdd->prepare("UPDATE article 
        SET titre = :titre, description = :description, photo = :photo
        WHERE id = :id");

        $req->execute(["titre"=> $article->getTitre(), "description"=> $article->getDescription(),
            'photo'=> $article->getPhoto(), "id"=> $article->getId()]);
    }
}